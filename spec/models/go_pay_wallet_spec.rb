require "spec_helper"
java_import com.gopay.client.models.responses.RegistrationResponse
java_import com.gopay.client.models.GoPayError

RSpec.describe Models::GoPayWallet do

  describe "#create_for_user" do
    pending "should create a go pay wallet for the user" do
      mocked_wallet_registration_response = double(RegistrationResponse)
      mocked_wallet_id = "mocked wallet"
      mocked_api_key = "api key"
      allow(mocked_wallet_registration_response).to receive(:wallet_id).and_return(mocked_wallet_id)
      allow(mocked_wallet_registration_response).to receive(:api_key).and_return(mocked_api_key)
      mocked_client = double(GoPayClient)
      allow(mocked_client).to receive(:register_customer).and_return(mocked_wallet_registration_response)
      wallet = Models::GoPayWallet.create_for_user(user)
      expect(wallet.id).to eq mocked_wallet_id
      expect(wallet.api_key).to eq mocked_api_key
    end

    pending "should return errors when creating a wallet fails" do
      mocked_wallet_id = "mocked wallet"
      mocked_api_key = "api key"
      mocked_wallet_registration_response = double(RegistrationResponse)
      allow(mocked_wallet_registration_response).to receive(:wallet_id).and_return(mocked_wallet_id)
      allow(mocked_wallet_registration_response).to receive(:api_key).and_return(mocked_api_key)
      allow(mocked_wallet_registration_response).to receive(:errors).and_return([GoPayError.new("308", user.phone)])
      mocked_client = double(GoPayClient)
      allow()
      allow(mocked_client).to receive(:register_customer).and_return(mocked_wallet_registration_response)
      wallet = Models::GoPayWallet.create_for_user(user)
      expect(wallet.has_any_errors?).to eq true
      expect(wallet.id).to be_empty
      expect(wallet.api_key).to be_empty
      expect(wallet.error_messages).to eq "308 - #{user.phone}"
    end
  end
end
