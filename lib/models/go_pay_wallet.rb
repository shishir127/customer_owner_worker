module Models
  class GoPayWallet

    attr_reader :id, :api_key

    def initialize(name, phone, email, api_key, error_messages)
      @id = id
      @api_key = api_key
      @errors = []
      @errors << error_messages
    end

    def self.create_for_user(user)
      go_pay_client = GoPayClient.new(ENV["GOPAY_HOST"], ENV["GOPAY_PORT"].to_i)
      registration_request = CustomerRegistrationRequest.new(user.name, user.phone, user.email,
        false, "sim", "928ri", "26c069ea-b060-4238-9413-84b6ac6e6718", "9pg", "007", "foobar", "foobar")
      response = go_pay_client.registerCustomer(registration_request)
      wallet = GoPayWallet.new(response.wallet_id, response.api_key, response.errors.inject("") { |message, error| message << (error.code + " - " + error.entity)  })
    end

    def valid?
      @errors.empty?
    end

    def error_messages
      @errors.join(", ")
    end
  end
end